require 'spec_helper'

describe TripEvent do
  it "does not have a road condition"
  it "has a location"
  it "belongs to a trip"
  it "has an order"
end
