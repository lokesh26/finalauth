require 'spec_helper'

describe Milestone do

  it "has a valid factory" do
    FactoryGirl.build(:milestone).should be_valid
  end

  it "has a valid road condition" do
    FactoryGirl.build(:milestone, road_condition: :good).should be_valid
    FactoryGirl.build(:milestone, road_condition: :bad).should be_valid
    FactoryGirl.build(:milestone, road_condition: :ugly).should_not be_valid
  end
end
