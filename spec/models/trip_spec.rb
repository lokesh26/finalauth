require 'spec_helper'

describe Trip do
  it "should have a valid factory" do
    trip = FactoryGirl.create(:trip)
    trip.should be_valid
  end

  it "should be invalid without a name" do
    FactoryGirl.build(:trip, name: nil).should_not be_valid
  end

  it "should have a valid means of transport" do
    FactoryGirl.build(:trip, transport: nil).should_not be_valid
    FactoryGirl.build(:trip, transport: :camel).should_not be_valid
    FactoryGirl.build(:trip, transport: :car).should be_valid
    FactoryGirl.build(:trip, transport: :other).should be_valid
    FactoryGirl.build(:trip, transport: :motorcycle).should be_valid
  end

  it "should belong to a user" do
    FactoryGirl.build(:trip, user: nil).should_not be_valid
  end

  it "should have at least 2 milestones" do
     FactoryGirl.create(:trip_with_milestones).milestones.count.should >= 2
  end

  it "should have a start and end location" do
    trip = FactoryGirl.build(:trip)
    m1 = FactoryGirl.create(:milestone, description: "Leaving from New York", trip: trip, order_position: :first)
    m2 = FactoryGirl.create(:milestone, description: "Going to LA", trip: trip, order_position: :last)
    m3 = FactoryGirl.create(:milestone, description: "Stopping at Chicago", trip: trip, order_position: 1)
    trip.milestones.first.should == m1
    trip.milestones.last.should == m2
  end
end
