require 'spec_helper'

describe User do
  it "has a valid factory" do
    FactoryGirl.build(:user).should be_valid
  end

  it "is invalid without a username" do
    FactoryGirl.build(:user, username: nil).should_not be_valid
  end

  it "is invalid with duplicate usernames" do
    FactoryGirl.create(:user, username: 'mogambo').should be_valid
    FactoryGirl.build(:user, username: 'Mogambo').should_not be_valid
  end

  it "is invalid with long and short usernames" do
    FactoryGirl.build(:user, username: 'tim').should_not be_valid
    FactoryGirl.build(:user, username: 'neil_nitin_mukesh').should_not be_valid
  end
  it "has gender either male or female" do
    FactoryGirl.build(:user, gender: :male).should be_valid
    FactoryGirl.build(:user, gender: :female).should be_valid
    FactoryGirl.build(:user, gender: :oops).should_not be_valid
  end

end
