# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    name { "#{Faker::Address.street_address} #{Faker::Address.city}, #{Faker::Address.state}, #{Faker::Address.country}" }
  end
end
