# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip do
    name "The best trip ever"
    transport :other
    published false
    user { build(:user) }
    factory :trip_with_milestones do
      ignore do 
        milestone_count 2
      end
      before(:create) do |trip, evaluator|
        create_list(:milestone, evaluator.milestone_count, trip: trip)
      end
    end
  end
end
