# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    n = Faker::Name.name
    full_name n
    username {Faker::Internet.user_name(n)[0, 15]}
    gender { [:male, :female].sample }
    description { Faker::Lorem.sentence }
    email { Faker::Internet.email(n) }
    birthdate { 30.years.ago }
    p = Faker::Internet.password
    password p
    password_confirmation p
  end
end
