# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :trip_event do
    visit_date { 10.days.ago }
    trip { build :trip }
    location { build :location }
    order_position 0
    factory :milestone, class: 'Milestone' do
      description "That's a milestone"
      road_condition { [:horrible, :bad, :good, :excellent].sample }
    end
  end
end
