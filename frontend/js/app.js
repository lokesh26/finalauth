// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs

// initialize foundation
$(document).foundation();

$(window).load(init);

function init() {
    var id = "me_trips";
    switch (id) {
        case "me_trips":
            me_trips.initialize();
            break;
    }

    common.initialize();
}

var utilities = {
    //Fill a HTML <select></select> with list of values in JavaScript array
    fillDropDown: function ($el, data, name, value) {
        for (var i = 0; i < data.length; i++) {
            /**
            * Fix #2: Updated method to create elements using jQuery, rather than raw HTML
            */
            $('<option/>').text(data[i][name]).attr('value', data[i][value]).appendTo($($el));
        }
    },

    //Fill a HTML <select></select> with list of values in JavaScript data object
    fillDropDown2: function ($el, data, nameKey, valueKey) {
        $.each(data, function (key, value) {
            if (valueKey) key = value[valueKey];
            if (nameKey) value = value[nameKey];
            $('<option/>').text(value).attr('value', key).appendTo($($el));
        });
    },

    //Method to create a HTML DOM element
    createElement: function (element, parent, attributes) {
        /**
        * @params
        *
        * element
        * Type: Selector
        * e.g. "<div/>"
        * String name of element which is required to be created
        *
        * parent
        * Type: String | Element
        * e.g. "#foo", $("#foo"), document.getElementById('foo')
        * Parent element of the newly created element
        *
        * attributes
        * Type: Object
        * e.g. {'className': 'bar', html: "Lorem Ipsum", id: "foo"}
        * Object of attributes to be applied to this element
        */

        if (!attributes) attributes = {};

        //create specified element
        var $el = $(element, attributes);

        //append this element to parent, if available
        if (parent) $el.appendTo($(parent));

        //return the newly created element
        return $el;
    },

    //Method to create JavaScript object of all fields in a <form></form>
    formToObject: function (form) {
        /*
        * Fix #3: Updated method to use jQuery rather that raw HTML
        */
        var $form = $(form),
            json = {};

        //Get Inputs
        $('input', $form).each(function (index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            name = $el.attr("name");
            switch ($el.attr("type")) {
                case 'checkbox':
                    //For checkboxes, we need to send values 0/1 rather than 'on', which is natively provided by HTML
                    $el.prop("checked") ? json[name] = "1" : json[name] = "0";
                    break;

                case 'radio':
                    if ($el.prop("checked")) json[name] = $el.val();
                    break;

                default:
                    json[name] = $el.val();
                    break;
            }
        });

        //Get Selects and textareas
        $('select,textarea', $form).each(function (index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            json[$el.attr("name")] = $el.val();
        });

        return json;
    },

    //Method for removal for multiple keys(in array) from a JavaScript Object
    dropKeysFromObject: function (obj, dropFields) {
        for (var i = 0; i < dropFields.length; i++) {
            if (obj[dropFields[i]]) delete obj[dropFields[i]];
        }

        return obj;
    },

    // load a javascript file from server
    loadJScript: function (fileName, callback) {
        var JSPATH = "/html/js/";

        $.getScript(JSPATH + fileName + ".js", callback);
    },

    //Method to load Google Maps API
    loadGMapScript: function (callback) {
        /*
        * Fix #4: Revised method to load Google Maps API - From Method 1 to Method 2
        * 
        * Google Maps API can be loaded using 2 methods
        * Method 1 creates a script tag and appends to document.body, e.g. $('<script/>').attr('src', "http://maps.googleapis.com/maps/api/js?sensor=false").appendTo(document.body);
        * Method 2 requests Google Maps API via a jQuery getScript method and calls all dependent methods henceforth
        */
        $.getScript("http://maps.googleapis.com/maps/api/js?sensor=false", function (data, textStatus, jqxhr) {
            if (callback) callback();
        });
    },

    /**
    * Create a new map, using Google Maps API, which should already be imported
    * Google Maps URL: http://maps.googleapis.com/maps/api/js?sensor=false
    */
    createGMap: function (el) {
        //if (typeof google.maps === "undefined") throw new Error("Google Maps required, but not imported.");

        var mapOptions = {
            zoom: 7,
            center: new google.maps.LatLng(27.219721, 78.019480),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDoubleClickZoom: true
        };

        //This method returns the Google Map object
        return new google.maps.Map(el, mapOptions);
    },

    /**
    * Set Autocomplete with Google Places
    * Google Maps with Place URL: http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places
    */
    setAutocompleteSearch: function (map, inputElement) {
        //if (typeof google.maps.places === "undefined") throw new Error("Google Places library required, but not imported.");

        var autocomplete = new google.maps.places.Autocomplete(inputElement);
        autocomplete.bindTo('bounds', map);

        return autocomplete;
    },

    /**
    * Get address of a location by its latitude and longitude. This method expects a callback which receives a formatted address string and address componets as params.
    * 
    * Requires Google Maps
    * URL: http://maps.googleapis.com/maps/api/js?sensor=false
    */
    getAddressByLatLng: function (latitude, longitude, callback) {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'location': new google.maps.LatLng(latitude, longitude)
        }, function (geocoderResult, geocoderStatus) {
            if (geocoderStatus == google.maps.GeocoderStatus.OK) {
                callback(geocoderResult[0].formatted_address, geocoderResult[0].address_components);
            } else {
                console.log("Google Geocoding Error: " + geocoderStatus);
            }
        });
    },

    //read photo from <input type="file" /> element($inputElement) and display it in <img src="" />($imageElement) tag
    readAndDisplayPhoto: function ($inputElement, $imageElement) {
        $inputElement = $($inputElement);

        if ($inputElement.prop('files') && $inputElement.prop('files')[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (ev) {
                $($imageElement).attr('src', ev.target.result);
            };
            fileReader.readAsDataURL($inputElement.prop('files')[0]);
        }
    },

    //standard error message format
    error: function (message) {
        return {
            'status': false,
            'message': message ? message : "UNKNOWN_ERROR"
        };
    },

    //standard success message format
    success: function (message) {
        return {
            'status': true,
            'message': message ? message : "OK"
        };
    },

    /*
    * this method uses an external plugin to set ellipsis
    * jquery.dot.dot.dot.min.js (https://github.com/FrDH/jQuery.dotdotdot)
    *
    * Provide a jQuery style selector to apply ellipsis
    */
    setdotdotdot: function (selector) {
        $(selector).dotdotdot({
            'ellipsis': "...",
            'wrap': "word",
            'fallbackToLetter': true,
            'after': null,
            'watch': true
        });
    },

    //convert JSON string to JavaScript object
    JSONtoObject: function (json) {
        if (JSON) return JSON.parse(json);
        else return eval('(' + json + ')');
    },

    /*
    * this method is used with an external plugin for detecting timezones
    * jstz.min.js (https://bitbucket.org/pellepim/jstimezonedetect)
    *
    * returns IANA zone info key
    */
    getTimezoneName: function () {
        var timezone = jstz.determine();
        return timezone.name();
    },

    //get cookie from browser, for key provided
    getCookieValue: function (key) {
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },

    //convert a data array to object mapper
    createMapFromArray: function (dataArray, key) {
        var mapper = {};
        if (!key) key = "id";
        for (var i = 0, l = dataArray.length; i < l; i++) {
            mapper[dataArray[i][key]] = dataArray[i];
        }

        return mapper;
    },

    //get number of keys in object
    getObjectLength: function (a) {
        return $.map(a, function (n, i) { return i; }).length;
    },

    //compare first string with second, upto n number of characters
    strncmp: function (a, b, n) {
        return a.substring(0, n) == b.substring(0, n);
    },

    // get parameter value from URL
    getURLVar: function (name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1];
    },

    // fill list of countries - defaults to 'US'
    fillCountries: function ($selector) {
        $selector = $($selector);
        this.fillDropDown2($selector, window.cz.countries_list, null, null);

        // set default country to United States
        $selector.val('US');
    },

    // fill list of currencies - defaults to 'USD'
    fillCurrencies: function ($selector) {
        var frequentCurrencies = {
            'USD': window.cz.currencies_list['USD'],
            'GBP': window.cz.currencies_list['GBP'],
            'EUR': window.cz.currencies_list['EUR'],
            'JPY': window.cz.currencies_list['JPY'],
            'CNY': window.cz.currencies_list['CNY'],
            'INR': window.cz.currencies_list['INR'],
            '#': { 'name': '------', 'country': '------', 'symbol': '------' }
        };
        utilities.fillDropDown2($selector, frequentCurrencies, "name", null);

        utilities.fillDropDown2($selector, window.cz.currencies_list, "name", null);

        // set default currency to USD
        $($selector).val('USD');
    },

    /**
    * Hash a string using SHA256 algorithm
    * 
    * Requires sha256.min.js
    * Crypto-js http://code.google.com/p/crypto-js/
    *
    * returns SHA256 hash
    */
    sha256: function (strval) {
        if (!CryptoJS.SHA256) {
            console.log("Crypto library SHA256 not included!");
            return false;
        }

        return CryptoJS.SHA256(strval).toString(CryptoJS.enc.Hex);
    },

    // extract bits from a 'value' from 'start_pos' to 'end_pos'
    extractBits: function (value, start_pos, end_pos) {
        var mask = (1 << (end_pos - start_pos)) - 1;
        return (value >> start_pos) & mask;
    },

    /**
    * Convert <textarea> fields or other HTML elements to RichText Editors
    *
    * Requires[CDN] //tinymce.cachefly.net/4.0/tinymce.min.js
    * Tinymce http://www.tinymce.com
    */
    richtextEditor: function (selector) {
        tinymce.init({ selector: selector, plugins: 'code' });
    },

    /**
    * Fill RichText Editors created by Tinymce
    *
    * Requires[CDN] //tinymce.cachefly.net/4.0/tinymce.min.js
    * Tinymce http://www.tinymce.com
    */
    fillRichText: function (elementID, value) {
        tinymce.get(elementID).setContent(value);
    },

    /**
    * Get RichText from Tinymce editor
    *
    * Requires[CDN] //tinymce.cachefly.net/4.0/tinymce.min.js
    * Tinymce http://www.tinymce.com
    */
    getRichText: function (elementID) {
        return tinymce.get(elementID).getContent();
    },

    /**
    * Add-on to jquery.validate.js
    *
    * Add method to check the file type extension specified in "rules"
    */
    addExtensionValidator: function (message) {
        jQuery.validator.addMethod("extension", function (value, element, param) {
            param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
            return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
        }, jQuery.format(message));
    },

    // scroll smoothly to top of page
    scrollToTop: function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    },

    /**
    * Get location of user from browser
    *
    * Requires
    * GeoPosition.js https://github.com/estebanav/javascript-mobile-desktop-geolocation
    */
    getBrowserLocation: function (successC, errorC) {
        // initialize geoPosition; location of user
        if (geoPosition.init()) {
            geoPosition.getCurrentPosition(successC, errorC, { enableHighAccuracy: true, timeout: 8000 });
        } else {
            errorC();
        }
    },

    /**
    * Let images load lazily
    *
    * Requires
    * jQuery.LazyLoad.js https://github.com/tuupola/jquery_lazyload/
    */
    goLazy: function ($selector) {
        if (!$selector) $selector = "img.lazy"; // default selector

        $($selector).lazyload({
            effect: "fadeIn"
        });
    }
};

var me_trips = {
    initialize: function () {
        
        var sampleFeedData = [{
            'userPic': "http://www.wallsave.com/wallpapers/800x600/people-face/305295/people-face-faces-x-actors-305295.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "http://blog.peerindex.com/wp-content/uploads/2012/08/coffee-beans.jpg",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "http://www.wallsave.com/wallpapers/800x600/people-face/305295/people-face-faces-x-actors-305295.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "http://blog.peerindex.com/wp-content/uploads/2012/08/coffee-beans.jpg",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "http://www.wallsave.com/wallpapers/800x600/people-face/305295/people-face-faces-x-actors-305295.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "http://blog.peerindex.com/wp-content/uploads/2012/08/coffee-beans.jpg",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "http://www.wallsave.com/wallpapers/800x600/people-face/305295/people-face-faces-x-actors-305295.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "http://blog.peerindex.com/wp-content/uploads/2012/08/coffee-beans.jpg",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "http://www.wallsave.com/wallpapers/800x600/people-face/305295/people-face-faces-x-actors-305295.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "http://blog.peerindex.com/wp-content/uploads/2012/08/coffee-beans.jpg",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        }];
        // sample feed printing
        this.printFeeds(sampleFeedData);
    
    
        // for drafts page
        if (utilities.getURLVar("_loc") === "drafts") {

        }
    },

    // create a feed HTML
    createFeed: function (feedDetails) {
        var $feed = utilities.createElement("<div/>", null, { 'class': "feed" });

        var $head = utilities.createElement("<div/>", $feed, { 'class': "head clearfix" });
        utilities.createElement("<div/>", $head, { 'class': "user-photo left", 'style': "background-image: url('" + feedDetails.userPic + "')" });
        utilities.createElement("<span/>", $head, { 'html': feedDetails.userName });
        utilities.createElement("<span/>", $head, { 'class': "views right", 'html': feedDetails.minsAgo + " mins ago &bull; " + feedDetails.noViews + " views" });

        var $photo = utilities.createElement("<div/>", $feed, { 'class': "photo" });
        utilities.createElement("<img/>", $photo, { 'src': feedDetails.tripPic, 'alt': "photo" });
        var $overlay = utilities.createElement("<div/>", $photo, { 'class': "overlay" });
        utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-edit tiny radius", 'html': "<span class='icon-edit'></span>&nbsp;&nbsp;EDIT" });
        utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-delete tiny radius", 'html': '<span class="icon-delete"></span>', 'data-reveal-id': "confirmDeleteModal" });

        var $content = utilities.createElement("<div/>", $feed, { 'class': "content" });
        utilities.createElement("<div/>", $content, { 'class': "trip-title", 'html': feedDetails.tripName });
        utilities.createElement("<p/>", $content, { 'html': feedDetails.tripDesc });

        utilities.createElement("<div/>", $feed, { 'class': "separator", 'html': "<hr />" });

        var $foot = utilities.createElement("<div/>", $feed, { 'class': "foot clearfix" });
        utilities.createElement("<span/>", $foot, { 'html': feedDetails.noMilestones + " milestones, " + feedDetails.noMoments + " moments, " + feedDetails.noPhotos + " photos" });
        var $right = utilities.createElement("<span/>", $foot, { 'class': "right" });
        utilities.createElement("<span/>", $right, { 'class': "like", 'html': '<span class="icon-like"></span>&nbsp;' + feedDetails.likes });
        utilities.createElement("<span/>", $right, { 'class': "comment", 'html': '<span class="icon-comment"></span>&nbsp;' + feedDetails.comments });

        return $feed;
    },

    /**SAMPLE AJAX call**/
    // AJAX call to fetch list of feeds
    fetchData: function (fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            url: "",
            success: function (data, textStatus, jqXHR) {
                // data array should be received here in JSON format

                try {
                    // convert JSON data to JS array/object
                    data = utilities.JSONtoObject(data);
                } catch (ex) {
                    console.log("invalid list of feed received from server.");
                    return;
                }

                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of feeds " + errorThrown);
            }
        });
    },

    // print list of feeds on page
    printFeeds: function (listOfFeeds) {
        var $container = $('.cols-wrapper');

        for (var i = 0, l = listOfFeeds.length; i < l; i++) {
            $container.append(this.createFeed(listOfFeeds[i]));
        }

        // masonrize feeds
        $container.masonry({
            // columnWidth: 160,
            "gutter": 18,
            itemSelector: '.feed'
        });
    }
};

var common = {
    initialize: function () {
        this.confirms();
        this.trip();
    },

    confirms: function () {
        // default configuration of confirmation modal
        $(".confirm").foundation("reveal", {
            animation: 'fadeAndFade',
            close_on_background_click: false
        });

        // add "CANCEL" handler to confirmation modal
        $(".cancel-confirm").on("click", function () {
            $(".confirm").foundation('reveal', 'close');
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm").on("click", function (ev) {
            // Send AJAX request to delete something

            // close modal if request completes successfully
            //$(".confirm").foundation('reveal', 'close');
        });
    },

    trip: function () {
        // handle like click
        $(".like").on("click", function () {
            if ($(this).hasClass("nav-text")) {
                $(this).removeClass("nav-text");

                /**Liked**/
                // AJAX call: like this trip
            } else {
                $(this).addClass("nav-text");

                /**Un-Liked**/
                // AJAX call: un-like this trip
            }
        });

        // handle comment click
        $(".comment").on("click", function () {
            window.location = "#";
        });
    }
};