Geokit::default_units = :kms
Geokit::default_formula = :sphere
Geokit::Geocoders::GoogleGeocoder.client_id = ''
Geokit::Geocoders::GoogleGeocoder.cryptographic_key = ''
Geokit::Geocoders::GoogleGeocoder.channel = ''
Geokit::Geocoders::provider_order = [:google]