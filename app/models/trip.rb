class Trip < ActiveRecord::Base
  extend FriendlyId
  belongs_to :user
  friendly_id :name, use: :scoped, scope: :user
  has_many :trip_events

  validates :name, presence: true
  validates :transport, inclusion: { in: [:car, :motorcycle, :bus, :bicycle, :other] }
  validates :user, presence: true

  validates_associated :trip_events
  delegate :milestones, to: :trip_events

  def transport
    read_attribute(:transport).to_sym
  end

  def transport=value
    write_attribute(:transport, value.to_s)
  end
end
