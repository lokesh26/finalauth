class Milestone < TripEvent

  validates :road_condition, presence: true, inclusion: { in: [:horrible, :bad, :good, :excellent] }

  def road_condition
    read_attribute(:road_condition).to_sym
  end

  def road_condition=value
    write_attribute(:road_condition, value.to_s)
  end

  def to_s
    "Milestone: #{description}"
  end

end