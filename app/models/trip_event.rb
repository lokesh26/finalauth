class TripEvent < ActiveRecord::Base
  include RankedModel

  default_scope { order(:order) }

  belongs_to :trip
  has_one :location, as: :locatable
  ranks :order, with_same: :trip_id

  validates :trip, presence: true
  validates :location, presence: true

  scope :milestones, -> { where(type: 'Milestone') }
end
