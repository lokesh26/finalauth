class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: :slugged

  has_many :trips
  has_many :authentications
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username,  presence: true,
                        length: {
                          minimum: 4,
                          maximum: 15
                        }
  validates :gender, inclusion: { in: [:male, :female] }

  def apply_omniauth(omniauth)
    authentications.build(:provider => omniauth['provider'], :uid => omniauth['uid'])
  end

  def gender
    read_attribute(:gender).to_sym
  end

  def gender=value
    write_attribute(:gender, value.to_s)
  end

  def password_required?
  (authentications.empty? || !password.blank?) && super
  end

end
