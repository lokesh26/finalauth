﻿// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs

$(window).load(init);

function init() {
    // initialize foundation
    $(document).foundation();

    switch (window.rmojo.id) {
        case "feed":
        case "me_trips":
            me_trips.initialize();
            break;

        case "me_followingPlaces":
            me_followingPlaces.initialize();
            break;

        case "profile_edit":
            profile_edit.initialize();
            break;

        case "profile_changePassword":
            profile_changePassword.initialize();
            break;

        case "profile_notifications":
            profile_notifications.initialize();

        case "me_followingFindInvite":
            me_followingFindInvite.initialize();

            break;
    }

    common.initialize();
}

var utilities = {
    //Fill a HTML <select></select> with list of values in JavaScript array
    fillDropDown: function ($el, data, name, value) {
        for (var i = 0; i < data.length; i++) {
            /**
            * Fix #2: Updated method to create elements using jQuery, rather than raw HTML
            */
            $('<option/>').text(data[i][name]).attr('value', data[i][value]).appendTo($($el));
        }
    },

    //Fill a HTML <select></select> with list of values in JavaScript data object
    fillDropDown2: function ($el, data, nameKey, valueKey) {
        $.each(data, function (key, value) {
            if (valueKey) key = value[valueKey];
            if (nameKey) value = value[nameKey];
            $('<option/>').text(value).attr('value', key).appendTo($($el));
        });
    },

    //Method to create a HTML DOM element
    createElement: function (element, parent, attributes) {
        /**
        * @params
        *
        * element
        * Type: Selector
        * e.g. "<div/>"
        * String name of element which is required to be created
        *
        * parent
        * Type: String | Element
        * e.g. "#foo", $("#foo"), document.getElementById('foo')
        * Parent element of the newly created element
        *
        * attributes
        * Type: Object
        * e.g. {'className': 'bar', html: "Lorem Ipsum", id: "foo"}
        * Object of attributes to be applied to this element
        */

        if (!attributes) attributes = {};

        //create specified element
        var $el = $(element, attributes);

        //append this element to parent, if available
        if (parent) $el.appendTo($(parent));

        //return the newly created element
        return $el;
    },

    //Method to create JavaScript object of all fields in a <form></form>
    formToObject: function (form) {
        /*
        * Fix #3: Updated method to use jQuery rather that raw HTML
        */
        var $form = $(form),
        json = {};

        //Get Inputs
        $('input', $form).each(function (index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            name = $el.attr("name");
            switch ($el.attr("type")) {
                case 'checkbox':
                    //For checkboxes, we need to send values 0/1 rather than 'on', which is natively provided by HTML
                    $el.prop("checked") ? json[name] = "1" : json[name] = "0";
                    break;

                case 'radio':
                    if ($el.prop("checked")) json[name] = $el.val();
                    break;

                default:
                    json[name] = $el.val();
                    break;
            }
        });

        //Get Selects and textareas
        $('select,textarea', $form).each(function (index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            json[$el.attr("name")] = $el.val();
        });

        return json;
    },

    //Method for removal for multiple keys(in array) from a JavaScript Object
    dropKeysFromObject: function (obj, dropFields) {
        for (var i = 0; i < dropFields.length; i++) {
            if (obj[dropFields[i]]) delete obj[dropFields[i]];
        }

        return obj;
    },

    // load a javascript file from server
    loadJScript: function (fileName, callback) {
        var JSPATH = "/html/js/";

        $.getScript(JSPATH + fileName + ".js", callback);
    },

    //Method to load Google Maps API
    loadGMapScript: function (callback) {
        /*
        * Fix #4: Revised method to load Google Maps API - From Method 1 to Method 2
        * 
        * Google Maps API can be loaded using 2 methods
        * Method 1 creates a script tag and appends to document.body, e.g. $('<script/>').attr('src', "http://maps.googleapis.com/maps/api/js?sensor=false").appendTo(document.body);
        * Method 2 requests Google Maps API via a jQuery getScript method and calls all dependent methods henceforth
        */
        $.getScript("http://maps.googleapis.com/maps/api/js?sensor=false", function (data, textStatus, jqxhr) {
            if (callback) callback();
        });
    },

    /**
    * Create a new map, using Google Maps API, which should already be imported
    * Google Maps URL: http://maps.googleapis.com/maps/api/js?sensor=false
    */
    createGMap: function (el) {
        //if (typeof google.maps === "undefined") throw new Error("Google Maps required, but not imported.");

        var mapOptions = {
            zoom: 7,
            center: new google.maps.LatLng(27.219721, 78.019480),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDoubleClickZoom: true
        };

        //This method returns the Google Map object
        return new google.maps.Map(el, mapOptions);
    },

    /**
    * Set Autocomplete with Google Places
    * Google Maps with Place URL: http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places
    */
    setAutocompleteSearch: function (map, inputElement) {
        //if (typeof google.maps.places === "undefined") throw new Error("Google Places library required, but not imported.");

        var autocomplete = new google.maps.places.Autocomplete(inputElement);
        autocomplete.bindTo('bounds', map);

        return autocomplete;
    },

    /**
    * Get address of a location by its latitude and longitude. This method expects a callback which receives a formatted address string and address componets as params.
    * 
    * Requires Google Maps
    * URL: http://maps.googleapis.com/maps/api/js?sensor=false
    */
    getAddressByLatLng: function (latitude, longitude, callback) {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'location': new google.maps.LatLng(latitude, longitude)
        }, function (geocoderResult, geocoderStatus) {
            if (geocoderStatus == google.maps.GeocoderStatus.OK) {
                callback(geocoderResult[0].formatted_address, geocoderResult[0].address_components);
            } else {
                console.log("Google Geocoding Error: " + geocoderStatus);
            }
        });
    },

    //read photo from <input type="file" /> element($inputElement) and display it in <img src="" />($imageElement) tag
    readAndDisplayPhoto: function ($inputElement, $imageElement) {
        $inputElement = $($inputElement);

        if ($inputElement.prop('files') && $inputElement.prop('files')[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (ev) {
                $($imageElement).attr('src', ev.target.result);
            };
            fileReader.readAsDataURL($inputElement.prop('files')[0]);
        }
    },

    /*
    * this method uses an external plugin to set ellipsis
    * jquery.dot.dot.dot.min.js (https://github.com/FrDH/jQuery.dotdotdot)
    *
    * Provide a jQuery style selector to apply ellipsis
    */
    setdotdotdot: function (selector) {
        $(selector).dotdotdot({
            'ellipsis': "...",
            'wrap': "word",
            'fallbackToLetter': true,
            'after': null,
            'watch': true
        });
    },

    //convert JSON string to JavaScript object
    JSONtoObject: function (json) {
        if (JSON) return JSON.parse(json);
        else return eval('(' + json + ')');
    },

    //get cookie from browser, for key provided
    getCookieValue: function (key) {
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },

    //convert a data array to object mapper
    createMapFromArray: function (dataArray, key) {
        var mapper = {};
        if (!key) key = "id";
        for (var i = 0, l = dataArray.length; i < l; i++) {
            mapper[dataArray[i][key]] = dataArray[i];
        }

        return mapper;
    },

    //get number of keys in object
    getObjectLength: function (a) {
        return $.map(a, function (n, i) { return i; }).length;
    },

    //compare first string with second, upto n number of characters
    strncmp: function (a, b, n) {
        return a.substring(0, n) == b.substring(0, n);
    },

    // get parameter value from URL
    getURLVar: function (name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1];
    },

    /**
    * Hash a string using SHA256 algorithm
    * 
    * Requires sha256.min.js
    * Crypto-js http://code.google.com/p/crypto-js/
    *
    * returns SHA256 hash
    */
    sha256: function (strval) {
        if (!CryptoJS.SHA256) {
            console.log("Crypto library SHA256 not included!");
            return false;
        }

        return CryptoJS.SHA256(strval).toString(CryptoJS.enc.Hex);
    },

    // extract bits from a 'value' from 'start_pos' to 'end_pos'
    extractBits: function (value, start_pos, end_pos) {
        var mask = (1 << (end_pos - start_pos)) - 1;
        return (value >> start_pos) & mask;
    },

    // scroll smoothly to top of page
    scrollToTop: function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
};

var me_trips = {
    initialize: function () {

        // for drafts page
        if (utilities.getURLVar("_loc") === "drafts") {
            this._isDraftsPage = true;
            $("#roadtripTab").removeClass("current");
            $("#draftsTab").addClass("current");
        }

        // for other's profile page
        if (utilities.getURLVar("_loc") === "otherProfile") {
            this._isOtherProfilePage = true;

            //$("#roadtripTab").removeClass("current");
            $("#draftsTab,#activityTab,#recommendedTab").next().remove();
            $("#draftsTab,#activityTab,#recommendedTab").remove();
        }

        var sampleFeedData = [{
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "John Doe",
            'minsAgo': "18",
            'noViews': "71",
            'tripPic': "",
            'mapLink': "http://maps.googleapis.com/maps/api/staticmap?center=-15.800513,-47.91378&zoom=15&size=600x450&sensor=false",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "15",
            'noMoments': "7",
            'noPhotos': "14",
            'likes': "5",
            'comments': "8"
        }];

        // sample feed printing
        this.printFeeds(sampleFeedData);
    },

    // Flag to indicate if current page is a drafts page
    _isDraftsPage: false,

    // Flag to indicate if current page is other's profile
    _isOtherProfilePage: false,

    // create a feed HTML
    createFeed: function (feedDetails) {
        var $feed = utilities.createElement("<div/>", null, { 'class': "feed" });

        var $head = utilities.createElement("<div/>", $feed, { 'class': "head clearfix" });
        utilities.createElement("<div/>", $head, { 'class': "user-photo left", 'style': "background-image: url('" + feedDetails.userPic + "')" });
        utilities.createElement("<span/>", $head, { 'html': feedDetails.userName });
        utilities.createElement("<span/>", $head, { 'class': "views right", 'html': feedDetails.minsAgo + " mins ago &bull; " + feedDetails.noViews + " views" });

        var $photo = utilities.createElement("<div/>", $feed, { 'class': "photo" });
        utilities.createElement("<img/>", $photo, { 'src': feedDetails.tripPic || feedDetails.mapLink, 'alt': "photo" });
        var $overlay = utilities.createElement("<div/>", $photo, { 'class': "overlay" });

        if (this._isDraftsPage) utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-edit tiny radius", 'html': "<span class='icon-edit'></span>&nbsp;&nbsp;EDIT & PUBLISH" });
        else utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-edit tiny radius", 'html': "<span class='icon-edit'></span>&nbsp;&nbsp;EDIT" });

        var $deleteButton = utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-delete tiny radius", 'html': '<span class="icon-delete"></span>', 'data-reveal-id': "confirmDeleteModal" });
        $deleteButton.on("click", function () {
            common._confirmsDelete = function () {
                //$feed.remove();
                $('.cols-wrapper').masonry('remove', $feed);
            };
        });

        var $content = utilities.createElement("<div/>", $feed, { 'class': "content" });
        utilities.createElement("<div/>", $content, { 'class': "trip-title", 'html': feedDetails.tripName });
        feedDetails.tripDesc && utilities.createElement("<p/>", $content, { 'html': feedDetails.tripDesc });

        utilities.createElement("<div/>", $feed, { 'class': "separator", 'html': "<hr />" });

        var $foot = utilities.createElement("<div/>", $feed, { 'class': "foot clearfix" });
        utilities.createElement("<span/>", $foot, { 'html': feedDetails.noMilestones + " milestones, " + feedDetails.noMoments + " moments, " + feedDetails.noPhotos + " photos" });

        if (!this._isDraftsPage) {
            var $right = utilities.createElement("<span/>", $foot, { 'class': "right" });
            utilities.createElement("<span/>", $right, { 'class': "like", 'html': '<span class="icon-like"></span>&nbsp;' + feedDetails.likes });
            utilities.createElement("<span/>", $right, { 'class': "comment", 'html': '<span class="icon-comment"></span>&nbsp;' + feedDetails.comments });
        }

        return $feed;
    },

    /**SAMPLE AJAX call**/
    // AJAX call to fetch list of feeds
    fetchData: function (fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            url: "",
            success: function (data, textStatus, jqXHR) {
                // data array should be received here in JSON format

                try {
                    // convert JSON data to JS array/object
                    data = utilities.JSONtoObject(data);
                } catch (ex) {
                    console.log("invalid list of feed received from server.");
                    return;
                }

                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of feeds " + errorThrown);
            }
        });
    },

    // print list of feeds on page
    printFeeds: function (listOfFeeds) {
        var $container = $('.cols-wrapper');

        for (var i = 0, l = listOfFeeds.length; i < l; i++) {
            $container.append(this.createFeed(listOfFeeds[i]));
        }

        // masonrize feeds
        $container.imagesLoaded(function () {
            $container.masonry({
                // columnWidth: 160,
                "gutter": 18,
                itemSelector: '.feed'
            });

            $container.masonry('on', 'removeComplete', function (msnryInstance, removedItems) {
                $container.masonry();
            });
        });
    }
};
/*me_followingFindInvite*/
var me_followingFindInvite = {
    initialize: function () {
        var self = this;

        // Currently Following Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.followingPeople = [
            { 'id': 1, 'name': "Mark Jenkins", 'roadtripsCount': "32", 'followingCount': "08", 'tripMiles':  "89", 'isBeingFollowed': true },
            { 'id': 2, 'name': "Sudeep MP", 'roadtripsCount': "101", 'followingCount': "08", 'tripMiles':  "89", 'isBeingFollowed': true },
            { 'id': 3, 'name': "Tushar", 'roadtripsCount': "22", 'followingCount': "08", 'tripMiles':  "89", 'isBeingFollowed': true },
            { 'id': 4, 'name': "Robin", 'roadtripsCount': "12", 'followingCount': "08", 'tripMiles':  "89", 'isBeingFollowed': true }
        ];

        //count the user being followed
        this.setFollowingPeopleCount();
         // If results are fetched successfully
        self.printRow(self.followingPeople);
    },
    followingPeople: [],
     // Create a search results row
    _createResultRow: function (resultDetails, followCallback, unfollowCallback) {
        var self = this;
        /*
        * Button Styles (Reference):
        *
        *
        * Following
        * <a href="#" class="hidden tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
        *
        * Follow
        * <a href="#" class="tiny button tiny-ex button-follow radius right"><span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow</a>
        *
        * Unfollow
        * <a href="#" class="hidden tiny button tiny-ex button-unfollow radius right"><span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;&nbsp;unfollow</a>
        */

        /*
        <div class="content-wrapper following-people">
              <div class="row">
               <div class="small-2 large-1 columns">
                <div class="user-photo"></div>
              </div>
              <div class="small-10 large-11 columns">
                <div class="following-people-details">
                  <div class="following-people-name">Mark Jenkins</div>
                  <div class="following-people-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...</div>
                  <div class="following-people-info">
                    <span>89 Miles</span>12 Roadtrips<span></span><span>189 Followers</span>
                  </div>
                </div>
                <a href="#" class="tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
              </div>
            </div>
          </div>
        */
        var $row = utilities.createElement("<div/>", null, { 'class': "content-wrapper following-people" });
        var $rowContainer = utilities.createElement("<div/>", $row, { 'class': "row" });
        var $col1 = utilities.createElement("<div/>", $rowContainer, { 'class': "medium-2 large-1 columns" });
        var $col2 = utilities.createElement("<div/>", $rowContainer, { 'class': "medium-10 large-11 columns" });
        var $followingPeopleDetails = utilities.createElement("<div/>", $col2, { 'class': "following-people-details" });


        utilities.createElement("<div/>", $col1, { 'class': "user-photo"});
        utilities.createElement("<div/>", $followingPeopleDetails, { 'class': "following-people-name", 'html': 'Mark Jenkins' });
        utilities.createElement("<div/>", $followingPeopleDetails, { 'class': "following-people-desc", 'html': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...' });
        utilities.createElement("<div/>", $followingPeopleDetails, { 'class': "following-people-desc", 'html': "<span>"+resultDetails.tripMiles+"Miles</span>"+resultDetails.roadtripsCount+" Roadtrips<span></span><span>"+resultDetails.followingCount+" Followers</span>" });

        // Following button - Grey with tick icon, converts to Unfollow on mouseover
        var $followingButton = utilities.createElement("<a/>", $col2, { 'href': "#", 'class': "hidden tiny button tiny-ex button-following radius right", 'html': '<span class="icon-tick-following links-text "></span>&nbsp;Following' });
        $followingButton.on("mouseover", function () {
            // On mouseover, 'Following' Button changes to 'Unfollow' Button
            $(this).html('<span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;Unfollow').removeClass('button-following').addClass('button-unfollow');

            // Add method to handle Unfollow button click
            $(this).on("click", function (ev) {
                ev.preventDefault();

                // Hide the Following/Unfollow button & show the Follow button
                $(this).hide();
                $followButton.show();

                // Method to be called when Unfollow button is clicked
                unfollowCallback && unfollowCallback.apply(self, [$row, resultDetails]);
            });
        });
        $followingButton.on("mouseout", function () {
            // On mouseout, 'Unfollow' Button changes to 'Following' Button
            $(this).html('<span class="icon-tick-following links-text "></span>&nbsp;Following').removeClass('button-unfollow').addClass('button-following');

            // Remove Unfollow button click handler
            $(this).off("click");
        });

        // Follow button - Green with + icon
        var $followButton = utilities.createElement("<a/>", $col2, { 'href': "#", 'class': "hidden tiny button tiny-ex button-follow radius right", 'html': '<span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow' });
        $followButton.on("click", function (ev) {
            ev.preventDefault();

            // Hide the Follow button & show the Following/Unfollow button
            $(this).hide();
            $followingButton.fadeIn();

            // Method to be called when Follow button is clicked
            followCallback && followCallback.apply(self, [$row, resultDetails]);
        });

        // Note: All buttons are hidden by default and should remain so. Please do not change this behaviour.

        // Initially display either Follow or Following button, depending on data received from server
        if (resultDetails.isBeingFollowed) {
            $followingButton.show();
        } else {
            $followButton.show();
        }

        return $row;
    },
    // Set number of people being followed
    setFollowingPeopleCount: function () {
        $("#followingPeopleCount").html(this.followingPeople.length);
    },

    /**
    * This method should be invoked, whenever this page is loaded
    * @params
    * searchResults - Array of search results/data result via ajax
    */
    printRow: function (listOfPeople) {
        var $resultsContainer = $("#followingContainer");

        for (var i = 0, l = listOfPeople.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(listOfPeople[i]));
        }
    },
};

var me_followingPlaces = {
    initialize: function () {
        var self = this;

        // Fetch & display Currently Following Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.followingPlaces = [
            { 'id': 1, 'location': "New Delhi, India", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': true },
            { 'id': 2, 'location': "Bangalore, India", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': true },
            { 'id': 3, 'location': "Mumbai, India", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': true },
            { 'id': 4, 'location': "Agra, India", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': true }
        ];

        // If results are fetched successfully
        this.setFollowingPlacesCount();
        self.searchComplete(self.followingPlaces, true);


        // Fetch & display Suggested Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.suggestedPlaces = [
            { 'id': 1, 'location': "New Delhi, India", 'roadtripsCount': "32", 'followingCount': "08", 'followersCount': "34", 'isBeingFollowed': false },
            { 'id': 2, 'location': "Bangalore, India", 'roadtripsCount': "05", 'followingCount': "38", 'followersCount': "34", 'isBeingFollowed': false },
            { 'id': 3, 'location': "Mumbai, India", 'roadtripsCount': "2", 'followingCount': "14", 'followersCount': "34", 'isBeingFollowed': false },
            { 'id': 4, 'location': "Agra, India", 'roadtripsCount': "71", 'followingCount': "25", 'followersCount': "34", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        this.setSuggestedPlacesCount();
        self.suggetionsFound(self.suggestedPlaces, true);




        // Instant search for places
        $("#placesSearch").on("keyup", Foundation.utils.throttle(function (e) {
            var $searchKeywords = $(this).val();

            if ($searchKeywords == "") {
                $("#searchFurther").hide();
                $("#searchResultsContainer").html("");
                return;
            }

            // AJAX call goes HERE

            // SAMPLE DATA (remove for production use):
            var searchResults = [
                { 'id': 9, 'location': "New Delhi, India", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': false },
                { 'id': 10, 'location': "Bangalore, India", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': false },
                { 'id': 11, 'location': "Mumbai, India", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': false },
                { 'id': 12, 'location': "Agra, India", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': false }
            ];

            // If results are fetched successfully
            self.searchComplete(searchResults, true);
        }, 300));

        // Further Search Results
        $("#searchFurther").on("click", function (ev) {
            ev.preventDefault();

            self.searchFurther();
        });

        // More suggestions
        $("#moreSuggestions").on("click", function (ev) {
            ev.preventDefault();

            self.moreSuggestions();
        });
    },

    followingPlaces: [],

    suggestedPlaces: [],

    // Set number of places being followed
    setFollowingPlacesCount: function () {
        $("#followingPlacesCount").html(this.followingPlaces.length);
    },

    // Set number of places being followed
    setSuggestedPlacesCount: function () {
        $("#suggestedPlacesCount").html(this.suggestedPlaces.length);
    },

    /**
    * This method should be invoked, whenever a search is complete
    * @params
    * searchResults - Array of search results
    * clearPreviousSuggestions - Flag (true/false) determining whether to clear previous search results or not
    */
    searchComplete: function (searchResults, clearPreviousResults) {
        var $resultsContainer = $("#searchResultsContainer");

        // clean up results container
        if (clearPreviousResults) $resultsContainer.html("");

        for (var i = 0, l = searchResults.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(searchResults[i], this._followCallback, this._unfollowCallback));
        }

        // Show container required for fetching more results
        $("#searchFurther").show();
    },

    /**
    * This method should be invoked, when suggestions for following places are found
    * @params
    * suggestions - Array of suggestions
    * clearPreviousSuggestions - Flag (true/false) determining whether to clear previous suggestions or not
    */
    suggetionsFound: function (suggestions, clearPreviousSuggestions) {
        var $resultsContainer = $("#suggestedContainer");

        // clean up results container
        if (clearPreviousSuggestions) $resultsContainer.html("");

        for (var i = 0, l = suggestions.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(suggestions[i], this._followCallback, this._unfollowCallback));
        }

        // Show container required for fetching more results
        $("#moreSuggestions").show();
    },

    // Create a search results row
    _createResultRow: function (resultDetails, followCallback, unfollowCallback) {
        var self = this;
        /*
        * Button Styles (Reference):
        *
        *
        * Following
        * <a href="#" class="hidden tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
        *
        * Follow
        * <a href="#" class="tiny button tiny-ex button-follow radius right"><span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow</a>
        *
        * Unfollow
        * <a href="#" class="hidden tiny button tiny-ex button-unfollow radius right"><span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;&nbsp;unfollow</a>
        */
        var $row = utilities.createElement("<div/>", null, { 'class': "content-wrapper" });
        var $textContent = utilities.createElement("<div/>", $row, { 'class': "place" });

        utilities.createElement("<p/>", $textContent, { 'class': "place-title", 'html': resultDetails.location });
        utilities.createElement("<p/>", $textContent, { 'class': "place-info", 'html': '<span>' + resultDetails.roadtripsCount + ' Roadtrips</span><span> ' + resultDetails.followingCount + ' Following</span>' + (resultDetails.followersCount?'<span>' + resultDetails.followersCount + ' Followers</span>':'') });

        // Following button - Grey with tick icon, converts to Unfollow on mouseover
        var $followingButton = utilities.createElement("<a/>", $row, { 'href': "#", 'class': "hidden tiny button tiny-ex button-following radius right", 'html': '<span class="icon-tick-following links-text "></span>&nbsp;Following' });
        $followingButton.on("mouseover", function () {
            // On mouseover, 'Following' Button changes to 'Unfollow' Button
            $(this).html('<span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;Unfollow').removeClass('button-following').addClass('button-unfollow');

            // Add method to handle Unfollow button click
            $(this).on("click", function (ev) {
                ev.preventDefault();

                // Hide the Following/Unfollow button & show the Follow button
                $(this).hide();
                $followButton.show();

                // Method to be called when Unfollow button is clicked
                unfollowCallback && unfollowCallback.apply(self, [$row, resultDetails]);
            });
        });
        $followingButton.on("mouseout", function () {
            // On mouseout, 'Unfollow' Button changes to 'Following' Button
            $(this).html('<span class="icon-tick-following links-text "></span>&nbsp;Following').removeClass('button-unfollow').addClass('button-following');

            // Remove Unfollow button click handler
            $(this).off("click");
        });

        // Follow button - Green with + icon
        var $followButton = utilities.createElement("<a/>", $row, { 'href': "#", 'class': "hidden tiny button tiny-ex button-follow radius right", 'html': '<span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow' });
        $followButton.on("click", function (ev) {
            ev.preventDefault();

            // Hide the Follow button & show the Following/Unfollow button
            $(this).hide();
            $followingButton.fadeIn();

            // Method to be called when Follow button is clicked
            followCallback && followCallback.apply(self, [$row, resultDetails]);
        });

        // Note: All buttons are hidden by default and should remain so. Please do not change this behaviour.

        // Initially display either Follow or Following button, depending on data received from server
        if (resultDetails.isBeingFollowed) {
            $followingButton.show();
        } else {
            $followButton.show();
        }

        return $row;
    },

    // Method to be invoked when Follow button is clicked
    _followCallback: function ($row, resultDetails) {
        // AJAX call goes HERE
        
        // If followed successfully
        this.followingPlaces.push(resultDetails);

        // set following places count again
        this.setFollowingPlacesCount();
    },

    // Method to be invoked when Follow button is clicked in Suggestions tab
    _suggestionsFollowCallback: function ($row, resultDetails) {
        // AJAX call goes HERE
        
        // If followed successfully
        this.followingPlaces.push(resultDetails);

        // set following places count again
        this.setFollowingPlacesCount();

        // Add this suggestion to following places tab
        //this.searchComplete([resultDetails], false);
    },

    // Method to be invoked when Unfollow button is clicked
    _suggestionsUnfollowCallback: function ($row, resultDetails) {
        var self = this;
        // AJAX call goes HERE

        // If un-followed successfully
        for(var i = 0, l = self.followingPlaces.length; i < l; i++) {
            if(self.followingPlaces[i]['id'] == resultDetails['id']) {
                self.followingPlaces.splice(i, 1);
                break;
            }
        }

        // set following places count again
        self.setFollowingPlacesCount();
    },

    // Fetch more results - Invoked when "Search Further" link below search results is clicked
    searchFurther: function () {
        var self = this;

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        var searchResults = [
            { 'id': 5, 'location': "NY, USA", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': false },
            { 'id': 6, 'location': "Gambia, Africa", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': false },
            { 'id': 7, 'location': "Paris, France", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': false },
            { 'id': 8, 'location': "Naples, Italy", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        self.searchComplete(searchResults, false);
    },

    // Fetch more suggestions - Invoked when "Show More Suggestions" link below suggestions is clicked
    moreSuggestions: function () {
        var self = this;

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        var suggestions = [
            { 'id': 5, 'location': "NY, USA", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': false },
            { 'id': 6, 'location': "Gambia, Africa", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': false },
            { 'id': 7, 'location': "Paris, France", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': false },
            { 'id': 8, 'location': "Naples, Italy", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        self.suggetionsFound(suggestions, false);
    }
};

/*Page: profile_edit.html*/
var profile_edit = {
    initialize: function () {
        var self = this;

        // Enable uploading a photo
        $("#uploadPhotoBtn").on("click", function () {
            $("#photo").click();
        });
        $("#photo").on("change", function () {
            $("#profilePhotoForm").submit();
        });

        // Handle form submit for profile info
        $("#profileInfoForm").validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function (form) {
                self.saveProfileInfo();

                return false;
            }
        });

        // Fetch & display existing user profile data
        this.fetchProfileInfo(this.displayProfileInfo);
    },

    // Fetch profile data from server
    fetchProfileInfo: function (fetchCallback) {
        // AJAX call goes HERE

        var profileData = {
            'name': "Tushar Agarwal",
            'location': "Bangalore",
            'weburl': "http://codeparallel.co",
            'bio': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, voluptas ipsam sequi ipsum odit perspiciatis dolor in odio illo iusto quo reprehenderit facilis cum dignissimos nesciunt explicabo est hic dolores.",
            'social': {
                'fb': true,
                'twitter': true,
                'gplus': false
            }
        };

        // If request was successful
        fetchCallback && fetchCallback.apply(self, [profileData]);
    },

    // Fill in profile info in textboxes
    displayProfileInfo: function(profileData) {
        $.each(profileData, function (key, value) {
            key = $("#" + key);
            if (key.is("input[type='checkbox']")) {
                key.prop("checked", +value);
            } else {
                key.val(value);
            }
        });
    },

    // Save user profile data
    saveProfileInfo: function () {
        var profileData = utilities.formToObject("#profileInfoForm");

        // AJAX call goes HERE

        // If AJAX call is complete & data is saved successfully, appropriate success message can be displayed here
    }
};

var profile_changePassword = {
    initialize: function () {

        // Focus current password textbox by default
        $("#currentPassword").focus();

                // Handle form submit for profile info
        $("#changePasswordForm").validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function (form) {
                self.sendChange();

                return false;
            }
        });
    },

    checkNewPasswords: function (newPassword1, newPassword2) {
        if(newPassword1 != "" && newPassword1 == newPassword2) return true;

        return false;
    },

    sendChange: function () {
        var passwords = utilities.formToObject("#changePasswordForm");

        if(passwords.currentPassword == "" || !this.checkNewPasswords(passwords.newPassword1, passwords.newPassword2)) {
            // New passwords do not match - display a message for the same
            
            return;
        }

        // AJAX call goes HERE

        // If AJAX call is complete & password is changed successfully, appropriate success message can be displayed here
    }
};

var profile_notifications = {
    initialize: function () {
        $("input[name='metric']").on("click", function () {
            if($(this).is(':checked')) {
                $(".checkIcon").html("");
                $(this).parent().find(".checkIcon").html('&nbsp;<span class="icon-tick-following"></span>');
            }
        });
    },

    // Save notification settings
    save: function () {
        var profileSettings = utilities.formToObject("#profileSettingsForm");

        // AJAX call goes HERE

        // If all settings are saved successfully, an appropriate success message can be triggered here
    }
};

var common = {
    initialize: function () {
        this.confirms();
        this.trip();
        this.tooltip();
        this.g_search();
    },

    // confirm & delete boxes
    confirms: function () {
        var self = this;

        // default configuration of confirmation modal
        $(".confirm").foundation("reveal", {
            animation: 'fadeAndFade',
            close_on_background_click: false
        });

        // add "CANCEL" handler to confirmation modal
        $(".cancel-confirm").on("click", function () {
            $(".confirm").foundation('reveal', 'close');
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm").on("click", function (ev) {
            // Send AJAX request to delete something
            // AJAX call goes HERE

            /* If request was sucessful: */
            // check & execute callback
            self._confirmsDelete && self._confirmsDelete();
            // Finally, close modal
            $(".confirm").foundation('reveal', 'close');
        });
    },

    // confirms-delete callback function (called when Delete button is pushed on a confirm dialog box)
    _confirmsDelete: null,

    // all trip commons are handled here
    trip: function () {
        // handle like click
        $(".like").on("click", function () {
            if ($(this).hasClass("nav-text")) {
                $(this).removeClass("nav-text");

                /**Liked**/
                // AJAX call: like this trip
            } else {
                $(this).addClass("nav-text");

                /**Un-Liked**/
                // AJAX call: un-like this trip
            }
        });

        // handle comment click
        $(".comment").on("click", function () {
            window.location = "#";
        });

        // Display profile pics of users - sidebar
        // Fetch & display
        this._fetchUsersPics(this._displayUsersPics);
    },

    // Fetch pics of users for sidebar
    _fetchUsersPics: function (fetchCallback) {
        var self = this;

        // SAMPLE DATA (remove for production use):
        var arrayOfImages = [
        "http://lorempixel.com/47/47/people/1",
        "http://lorempixel.com/47/47/people/2",
        "http://lorempixel.com/47/47/people/3",
        "http://lorempixel.com/47/47/people/4",
        "http://lorempixel.com/47/47/people/5",
        "http://lorempixel.com/47/47/people/6",
        "http://lorempixel.com/47/47/people/7",
        "http://lorempixel.com/47/47/people/8",
        "http://lorempixel.com/47/47/people/9",
        "http://lorempixel.com/47/47/people/10",
        "http://lorempixel.com/47/47/people/7",
        "http://lorempixel.com/47/47/people/8",
        "http://lorempixel.com/47/47/people/9",
        "http://lorempixel.com/47/47/people/10",
        "http://lorempixel.com/47/47/people/7"
        ];

        // Fetch img paths of all user photos
        // AJAX call goes HERE

        // If request completes successfully:

        // Execute callback - with array of img paths
        fetchCallback && fetchCallback.apply(self, [arrayOfImages]);
    },

    // Display pics of users in sidebar
    _displayUsersPics: function (arrayOfImages) {
        //debugger;
        var picsInARow = 6,
        maxRows = 2,
        imgCount = arrayOfImages.length,
        $imagesContainer = $(".profile-pics"),
        addPic = function (imgPath) {
            utilities.createElement("<img/>", $imagesContainer, { 'src': imgPath, 'alt': "" });
        },
        i = 0;

        if (imgCount <= picsInARow) {
            // Total number of followers is less than or equal to total number of pics required in a row

            // loop through imgCount and display images
            for (; i < imgCount; i++) {
                addPic(arrayOfImages[i]);
            }
        } else if (imgCount > picsInARow) {
            // Total number of followers is greater than total number of pics required all rows

            // calculate number of images that should be displayed and number of remaining images
            var remainingCount = (imgCount + 1) % picsInARow,
            displayCount = imgCount - remainingCount,
            rows = Math.ceil(displayCount / picsInARow),
            rCount = 0;

            // If number of rows that are going to be displayed exceed the maximum number of allowed rows
            if (rows > maxRows) {
                var excessPics = (rows - maxRows) * picsInARow;
                displayCount -= excessPics;
                remainingCount += excessPics;
            }

            // loop through picsInARow and display images
            for (; rCount < rows; rCount++) {
                for (; i < displayCount; i++) {
                    addPic(arrayOfImages[i]);
                }
            }

            // Display number of excess images
            utilities.createElement("<span/>", $imagesContainer, { 'class': "more left text-center", 'html': "+" + remainingCount });
        }
    },

    // custom tooltip
    tooltip: function () {
        $(document).foundation({
            tooltips: {
                selector: '.has-tip',
                additional_inheritable_classes: [],
                tooltip_class: '.tooltip',
                touch_close_text: 'tap to close',
                disable_for_touch: false,
                tip_template: function (selector, content) {
                    return '<span data-selector="' + selector + '" class="'
                    + Foundation.libs.tooltips.settings.tooltipClass.substring(1)
                    + '">' + content + '<span class="nub"></span></span>';
                }
            }
        });
    },

    g_search: function () {
        var self = this;

        if(!this.searchOverlay) this.searchOverlay = this.elasticSearch();

        $(".elasticSearchTrigger").on("click", function () {
            $("body").append(self.searchOverlay);
        });
    },

    searchOverlay: null,

    // Create elastic search box/overlay
    elasticSearch: function () {
        var $searchWrap = utilities.createElement("<div/>", null, { 'class': "search-wrapper" });
        var $searchHeading = utilities.createElement("<div/>", $searchWrap, { 'class': "search-heading" });
        var $col = utilities.createElement("<div/>", null, { 'class': "medium-12 large-12 column no-padding-left" }).appendTo(utilities.createElement("<div/>", $searchHeading, { 'class': "row" }));
        utilities.createElement("<input/>", $col, { 'class': "place-search main", 'type': "search", 'placeholder': "Search for places..." });
        var $searchContent = utilities.createElement("<div/>", null, { 'class': "search-content" }).appendTo(utilities.createElement("<div/>", $searchWrap, { 'class': "search-container" }));
        
        var $contentWrapper = utilities.createElement("<div/>", $searchContent, { 'class': "row content-wrapper" })
        utilities.createElement("<div/>", null, { 'class': "search-place-result", 'html': "New Delhi, India" }).appendTo(utilities.createElement("<div/>", $contentWrapper, { 'class': "medium-7 large-7 column content-inner-wrapper" }));

        $col = utilities.createElement("<div/>", $contentWrapper, { 'class': "medium-5 large-5 column" });
        $col = utilities.createElement("<div/>", null, { 'class': "large-12 column clearfix" }).appendTo(utilities.createElement("<div/>", $col, { 'class': "row" }));
        utilities.createElement("<a/>", $col, { 'href': "#", 'class': "tiny button tiny-ex button-follow radius right", 'html': '<span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow' });
        utilities.createElement("<div/>", $col, { 'class': "place-info text-right right", 'html': "32 Roadtrips, 34 Followers" });

        return $searchWrap;
    }
};