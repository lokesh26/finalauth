class CreateTripEvents < ActiveRecord::Migration
  def change
    create_table :trip_events do |t|
      t.datetime :visit_date
      t.text :description
      t.references :trip, index: true
      t.string :type, null: false
      t.integer :order
      t.string :road_condition

      t.timestamps
    end
  end
end
