class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.float :lat
      t.float :lng
      t.references :locatable, polymorphic: true, index: true

      t.timestamps
    end
    add_index :locations, [:lat, :lng], unique: true
  end
end
