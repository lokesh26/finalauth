class AddPropertiesToUser < ActiveRecord::Migration
  def change
    add_column :users, :username, :string, null: false
    add_index :users, :username, unique: true
    add_column :users, :full_name, :string
    add_column :users, :birthdate, :date
    add_column :users, :gender, :string, null: false, default: 'male'
    add_column :users, :description, :text
  end
end
