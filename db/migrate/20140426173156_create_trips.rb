class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :name, null: false
      t.text :description
      t.string :transport, null: false, default: 'other'
      t.boolean :published, null: false, default: false
      t.string :slug, null: false
      t.belongs_to :user, index: true, null: false

      t.timestamps
    end
    add_index :trips, :slug, unique: true
  end
end
